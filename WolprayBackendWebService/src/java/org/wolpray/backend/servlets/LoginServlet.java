package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.pojo.User;
import org.cat.proven.wolprayproject.models.persist.UserDao;

/**
 *
 * @author Lewis
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/loginservlet/*"})
@MultipartConfig
public class LoginServlet extends HttpServlet {

    private UserDao userDao;
    private Utils utils;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            userDao = new UserDao();
            utils = new Utils();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Error con la conexión de la base de datos");
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Opción inválida", -1);

        } else {
            String[] splits = pathInfo.split("/");
            String action = splits[1];

            switch (action) {
                case "login":
                    String mail = splits[2];
                    String password = splits[3];
                    login(mail, password, response);
                    break;
                case "valemail":
                    String mailtovale = splits[2];
                    validateEmail(mailtovale, response);
                    break;
                case "logout":
                    String id = splits[2];
                    logoutUser(id, response);
                    break;
                default:
                    writeResponse(response, "Esta no es una opción válida", -1);
                    break;
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();
        System.out.println("Estoy en el post");

        if (pathInfo == null || pathInfo.equals("/")) {

            User user = getJsonValues(request);
            if (user != null) {
                registrerUser(user, response, request);
            } else {
                writeResponse(response, "No has enviado ningún objeto", -1);
            }

        } else {
            writeResponse(response, "Acción inválida", -1);
        }
    }

    private void login(String mail, String password, HttpServletResponse response) {

        User user = userDao.findUserByEmail(mail);
        if (user != null) {
            if (user.getPassword().equals(password) || user.getMail().equals(mail)) {
                if (user.getRole().equalsIgnoreCase("client")) {
                    if (userDao.validateSession(user)) {
                        writeResponse(response, user, 1);
                    } else {
                        writeResponse(response, "Oops, parece que quieres entrar dos veces a la plataforma", 0);
                    }
                } else {
                    writeResponse(response, user, 1);
                }
            } else {
                writeResponse(response, "Correo electrónico o contraseña incorrecta", -1);
            }

        } else {
            writeResponse(response, "No nos consta este usuario en la base de datos...", -2);
        }

    }

    private int validateUser(User user) {
        int result;
        User oldUser = userDao.findUserByUserName(user.getUserName());
        if (oldUser != null) {
            result = -3;
        } else {
            oldUser = userDao.findUserByEmail(user.getMail());
            if (oldUser != null) {
                result = -2;
            } else {
                oldUser = userDao.findUserByPhone(user.getPhone());
                if (oldUser != null) {
                    result = -1;
                } else {
                    int yearsOld = getYearsOld(user.getBirthDate());

                    if (yearsOld >= 18) {
                        result = userDao.addUser(user);
                    } else {
                        result = 0;
                    }
                }
            }
        }
        return result;
    }

    private void validateEmail(String mail, HttpServletResponse response) {
        User user = userDao.findUserByEmail(mail);
        if (user != null) {
            writeResponse(response, user, 1);

        } else {
            writeResponse(response, "Este usuario no existe en la base de datos", -1);
        }
    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void logoutUser(String id, HttpServletResponse response) {

        try {
            int ids = Integer.parseInt(id);
            User user = userDao.findUserById(ids);
            if (user != null) {
                boolean isLogout = userDao.logoutUser(user);
                if (isLogout) {
                    writeResponse(response, "El usuario ha cerrado sesión", 1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de cerrar sesión en la base de datos", -1);
                }

            } else {
                writeResponse(response, "Este usuario no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }

    }

    private int getYearsOld(String birthDate) {
        String split[] = birthDate.split("-");
        int yearInt = Integer.parseInt(split[0]);
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int yearsOld = currentYear - yearInt;
        return yearsOld;
    }

    private User getJsonValues(HttpServletRequest request) throws IOException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();

        User user = gson.fromJson(payload, User.class);

        return user;
    }

    private void registrerUser(User user, HttpServletResponse response, HttpServletRequest request) {
        int id = userDao.getLastId();
        id++;
        if(userDao.findUserById(id) != null){
            id++;
        }
        user.setId(id);
        
        
        int isAdded = validateUser(user);
        switch (isAdded) {
            case -3:
                writeResponse(response, "No puedes registrarte con este nombre de usuario porque ya está siendo usado", -3);
                break;
            case -2:
                writeResponse(response, "No puedes registrarte con este correo electrónico porque ya está siendo usado", -2);
                break;
            case -1:
                writeResponse(response, "No puedes registrarte con este teléfono porqe ya esta siendo usado", -1);
                break;
            case 0:
                writeResponse(response, "Tienes que ser mayor de edad", 0);
                break;
            case 1:

                writeResponse(response, "¡Te has registrado con éxito!", 1);

                break;
            default:
                writeResponse(response, "Un error insperado ha ocurrido", -3);
                break;
        }
    }

}
