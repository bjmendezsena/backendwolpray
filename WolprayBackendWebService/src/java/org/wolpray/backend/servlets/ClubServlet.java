package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.ClubDao;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.pojo.Club;


/**
 *
 * @author Lewis
 */
@WebServlet(name = "ClubServlet", urlPatterns = {"/clubservlet/*"})
@MultipartConfig
public class ClubServlet extends HttpServlet {

    private ClubDao clubDao;
    private Utils utils;
    private final String HOST_PATH = "http://46.101.236.229:8080";
    private String roothPath;
    private String roothPhotoPath;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            clubDao = new ClubDao();
            utils = new Utils();
            this.roothPath = getServletContext().getContextPath();
            System.out.println("Voy a coger el path");
            this.roothPhotoPath = getServletContext().getRealPath("/") + "fotos";
            System.out.println("EL path es: "+this.roothPhotoPath);
        } catch (ClassNotFoundException ex) {
            System.out.println("Fallo en la conexión a la base de datos");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        System.out.println("Estoy en el get");
        if (pathInfo == null || pathInfo.equals("/")) {
            listAllClubs(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 3:
                    String param1 = splits[1];
                    String param2 = splits[2];
                    listByAnyWhere(param1, param2, response);
                    break;
                case 2:
                    String paramether = splits[1];
                    findClubById(response, paramether);
                    break;
                default:
                    writeResponse(response, "Opción inválida", -1);
                    break;
            }

        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        System.out.println("Entre al put");
        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    String id = splits[1];

                    try {
                        int ids = Integer.parseInt(id);

                        Club club = clubDao.findClubById(ids);
                        if (club != null) {
                            modifyClub(response, club, request);
                        } else {
                            writeResponse(response, "Este club no existe en a base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id ha de ser un campo válido. id: " + id, -1);
                    }
                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String pathInfo = request.getPathInfo();
        System.out.println("Estoy en el post");

        if (pathInfo == null || pathInfo.equals("/")) {

            Club club = getJsonValues(request);

            if (club != null) {
                addClub(club, response);
            } else {
                writeResponse(response, "No has enviado ningún objeto", -1);
            }

        } else {
            writeResponse(response, "Acción inválida", -1);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        }

        String[] splits = pathInfo.split("/");

        if (splits.length != 2) {
            writeResponse(response, "Acción no válida", -1);
        }

        String id = splits[1];

        try {
            long ids = Long.parseLong(id);
            Club club = clubDao.findClubById(ids);
            if (club != null) {
                if (clubDao.deleteClub(club)) {
                    if (utils.deleteFile(getServletContext().getRealPath("/") + club.getCoverUrl())) {
                        writeResponse(response, "Se ha eliminado el club con éxito", -1);

                    } else {
                        writeResponse(response, "Ha ocurrido un error al tratar de borrar la foto del club", -1);
                    }
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de borrar el club", -1);
                }
            } else {
                writeResponse(response, "El club no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El campo id no es válido", -1);
        }

    }

    private void modifyClub(HttpServletResponse response, Club club, HttpServletRequest request) {

        try {

            Club newClub = getJsonValues(request);

            if (newClub != null) {

                if (newClub.getName() != null && newClub.getName().length() > 0) {
                    club.setName(newClub.getName());
                }
                if (newClub.getStreetName() != null && newClub.getStreetName().length() > 0) {
                    club.setStreetName(newClub.getStreetName());
                }

                if (newClub.getStreetNumber() != null && newClub.getStreetNumber().length() > 0) {
                    club.setStreetNumber(newClub.getStreetNumber());
                }

                if (newClub.getPostalCode() != null && newClub.getPostalCode().length() > 0) {
                    club.setPostalCode(newClub.getPostalCode());
                }

                if (newClub.getCity() != null && newClub.getCity().length() > 0) {
                    club.setCity(newClub.getCity());
                }

                if (newClub.getDescription() != null && newClub.getDescription().length() > 0) {
                    club.setDescription(newClub.getDescription());
                }

                if (newClub.getAmbience() != null && newClub.getAmbience().length() > 0) {
                    club.setAmbience(newClub.getAmbience());
                }

                if (newClub.getPhone() != null && newClub.getPhone().length() > 0) {
                    club.setPhone(newClub.getPhone());
                }

                if (newClub.getDressCode() != null && newClub.getDressCode().length() > 0) {
                    club.setDressCode(newClub.getDressCode());
                }

                if (newClub.getLatitude() != null && newClub.getLatitude().length() > 0) {
                    club.setLatitude(newClub.getLatitude());
                }

                if (club.getLongitude() != null && club.getLongitude().length() > 0) {
                    club.setLongitude(newClub.getLongitude());
                }
                if (newClub.getCoverUrl() != null && newClub.getCoverUrl().length() > 0) {
                    utils.modifyImage(this.roothPhotoPath, club.getCoverUrl(), newClub.getCoverUrl());
                }

            }

            if (clubDao.modifyClub(club)) {
                writeResponse(response, "El club se ha modificado con éxito", 1);
            } else {
                writeResponse(response, "Ha ocurrido un error al tratar de modificar el club", -1);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el reader", -1);
        } catch (ServletException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el servlet", -1);
        }
    }

    private Club getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println(payload);

        Club club = gson.fromJson(payload, Club.class);

        return club;
    }

    private void findClubById(HttpServletResponse response, String paramether) {

        try {
            long ids = Long.parseLong(paramether);
            Club club = clubDao.findClubById(ids);
            if (club != null) {
                club.setCoverUrl(this.HOST_PATH + getServletContext().getContextPath() + club.getCoverUrl());
                writeResponse(response, club, 1);
            } else {
                writeResponse(response, "Este club no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {

            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }
    }

    private void findClubByManager(HttpServletResponse response, String id) {
        try {
            int idm = Integer.parseInt(id);
            List<Club> list = clubDao.findClubsByManager(idm);
            if (list != null && list.size() > 0) {
                for (Club club : list) {
                    club.setCoverUrl(this.HOST_PATH + club.getCoverUrl());
                }
                writeResponse(response, list, 1);
            } else {
                writeResponse(response, "Este usuario no tiene discotecas manejando", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El id del usuario tiene que ser un campo válido", -1);
        }
    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void listAllClubs(HttpServletResponse response) {

        List<Club> list = clubDao.findAllClubs();

        if (list != null) {
            for (Club club : list) {
                club.setCoverUrl(this.HOST_PATH + this.roothPath+ club.getCoverUrl());
            }

            writeResponse(response, list, 1);

        } else {
            writeResponse(response, "Ha ocurrido un error al tratar de buscar los clubs", 0);
        }

    }

    private int validateClub(Club club) {
        int result = -1;

        Club oldClub = clubDao.findClubByName(club.getName());
        String image64 = club.getCoverUrl();

        club.setCoverUrl("/assets/clubs/"+club.getId()+".jpg");

        if (oldClub == null) {
            if (clubDao.findClubById(club.getId()) != null) {
                long id = club.getId() + 1;
                club.setId(id);
            }
            System.out.println(club.getCoverUrl());
            if (clubDao.addNewClub(club)) {
                if (utils.createImageFromBase64(image64, this.roothPhotoPath, "clubs", club.getId() + ".jpg")) {
                    club.setCoverUrl(utils.getPathForBDd());
                    result = 1;
                } else {
                    clubDao.deleteClub(club);
                    result = -1;
                }

            } else {
                result = 0;
            }
        } else {
            result = -1;
        }

        return result;
    }

    private void addClub(Club club, HttpServletResponse response) throws IOException, ServletException {

        long id = clubDao.getClubId();

        id++;
        club.setId(id);

        System.out.println("Voy a validar");
        int result = validateClub(club);
        switch (result) {
            case -1:
                writeResponse(response, "Ya existe otro club con este nombre", -1);
                break;
            case -2:
                writeResponse(response, "Problemas al tratar de subir la imagen del club " + club.getName(), -1);
                break;
            case 0:
                writeResponse(response, "Ha ocurrido un error al tratar de añadir el club a la base de datos", -1);
                break;
            case 1:
                writeResponse(response, "El club se ha añadido correctamente", 1);
                break;
            default:
                writeResponse(response, "Ha ocurrido un error inesperado", 1);
                break;
        }

    }

    private void listByAnyWhere(String param1, String param2, HttpServletResponse response) {

        switch (param1) {
            case "manager":
                findClubByManager(response, param2);
                break;
            case "city":
                findClubByCity(response, param2);
                break;
            case "ambience":
                findClubByAmbience(response, param2);
                break;
            case "dressCode":
                findClubByDressCode(response, param2);
                break;
            default:
                writeResponse(response, "Opción inválida", -1);
                break;

        }
    }

    private void findClubByCity(HttpServletResponse response, String param) {

        List<Club> list = clubDao.findClubsBycity(param);
        if (list != null && list.size() > 0) {
            for(Club club: list){
                    club.setCoverUrl(this.HOST_PATH+club.getCoverUrl());
                }
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "No existen clubs en esta ciudad", -1);
        }
    }

    private void findClubByAmbience(HttpServletResponse response, String param) {
        List<Club> list = clubDao.findClubsByAmbience(param);
        if (list != null && list.size() > 0) {
            for (Club club : list) {
                club.setCoverUrl(this.HOST_PATH + club.getCoverUrl());
            }
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "No existen clubs en esta ciudad", -1);
        }

    }

    private void findClubByDressCode(HttpServletResponse response, String param) {
        List<Club> list = clubDao.findClubsByDressCode(param);
        if (list != null && list.size() > 0) {
            for (Club club : list) {
                club.setCoverUrl(this.HOST_PATH + club.getCoverUrl());
            }
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "No existen clubs en esta ciudad", -1);
        }
    }

}
