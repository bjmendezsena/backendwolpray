package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.ClubDao;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.persist.ProductDao;
import org.cat.proven.wolprayproject.models.persist.PromotionDao;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Product;
import org.cat.proven.wolprayproject.models.pojo.Promotion;

/**
 *
 * @author Lewis
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/productservlet/*"})
@MultipartConfig
public class ProductServlet extends HttpServlet {

    private ProductDao productDao;
    private ClubDao clubDao;
    private PromotionDao promotionDao;
    private Utils utils;
    private final String HOST_PATH = "http://46.101.236.229:8080";
    private String roothPath;
    private String roothPhotoPath;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            productDao = new ProductDao();
            clubDao = new ClubDao();
            promotionDao = new PromotionDao();
            utils = new Utils();
            this.roothPath = getServletContext().getContextPath();
            this.roothPhotoPath = getServletContext().getRealPath("/") + "assets";
        } catch (ClassNotFoundException ex) {
            System.out.println("Conexión en la base de datos fallida");
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            listAllProducts(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 3:
                    String param1 = splits[1];
                    String param2 = splits[2];
                    getProductsByAnyWhere(param1, param2, response);
                    break;
                case 2:
                    String paramether = splits[1];
                    try {
                        int ids = Integer.parseInt(paramether);
                        findProductById(response, ids);
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id tiene que ser un parámetro válido", 0);
                    }
                    break;
                default:
                    writeResponse(response, "Opción inválida", -1);
                    break;
            }

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        System.out.println("Estoy en el post");

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción inválida", -1);

        } else {
            String[] splits = pathInfo.split("/");
            System.out.println("Estoy recogiendo el id del club");

            switch (splits.length) {
                case 2:
                    try {
                    int id = Integer.parseInt(splits[1]);
                        System.out.println("El id es: "+id);
                    Club club = clubDao.findClubById(id);
                    
                    if (club != null) {
                        System.out.println("El club es "+club);
                        addNewProduct(request, response, club);
                    } else {
                        writeResponse(response, "No existe ningún club con esta id", -1);
                    }
                } catch (NumberFormatException e) {
                    writeResponse(response, "El id del club tiene que ser un campo válido", -1);
                }

                break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        }

        String[] splits = pathInfo.split("/");

        if (splits.length != 2) {
            writeResponse(response, "Acción no válida", -1);
        }

        String id = splits[1];

        try {
            long ids = Long.parseLong(id);
            Product product = productDao.findProductById(ids);
            if (product != null) {
                if (productDao.deleteProduct(product)) {
                    if (utils.deleteFile(getServletContext().getRealPath("/") + product.getImageUrl())) {
                        writeResponse(response, "Se ha eliminado el producto con éxito", -1);

                    } else {
                        writeResponse(response, "Ha ocurrido un error al tratar de borrar la foto del producto", -1);
                    }
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de borrar el producto de la base de datos", -1);
                }
            } else {
                writeResponse(response, "El producto no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El campo id no es válido", -1);
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    String id = splits[1];

                    try {
                        int ids = Integer.parseInt(id);

                        Product product = productDao.findProductById(ids);
                        if (product != null) {
                            modifyProduct(product, response, request);

                        } else {
                            writeResponse(response, "Este producto no existe en a base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id ha de ser un campo válido. id: " + id, -1);
                    }
                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }

    }


    private void listAllProducts(HttpServletResponse response) {
        List<Product> list = productDao.findAllProducts();

        if (list != null) {
            for (Product product : list) {
                product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
            }

            writeResponse(response, list, 1);

        } else {
            writeResponse(response, "Ha ocurrido un error al tratar de buscar los clubs", 0);
        }
    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private int validateProduct(Product product) {
        int result = -1;

        String image64 = product.getImageUrl();
        product.setImageUrl("/assets/products/"+product.getId()+".jpg");

        if (product.getPrice() <= 0) {
            result = -1;
        } else {
            if (productDao.findProductById(product.getClubId()) != null) {
                product.setId(product.getClubId() + 1);
            }
            if (productDao.addProduct(product)) {
                if (utils.createImageFromBase64(image64, this.roothPhotoPath, "products", product.getId() + ".jpg")) {
                    result = 1;
                } else {
                    productDao.deleteProduct(product);
                    result = 2;
                }
            } else {

            }
        }

        return result;

    }


    private void addNewProduct(HttpServletRequest request, HttpServletResponse response, Club club) throws IOException, ServletException {
        System.out.println("Voy a recoger los datos");
        Product product = getJsonValues(request);

        if (product != null) {
            long id = productDao.getLastProductId();

            id++;
            product.setId(id);
            product.setClubId(club.getId());
            int isAdded = validateProduct(product);
            switch (isAdded) {
                case -1:
                    writeResponse(response, "El precio del producto no es el correcto", -1);
                    break;
                case 0:
                    writeResponse(response, "Ha ocurrido un error al tratar de añadir el producto", -1);
                    break;
                case 1:
                    writeResponse(response, "El producto se ha añadido correctamente", 1);
                    break;
                default:
                    writeResponse(response, "Ha ocurrido un error inesperado", -1);
                    break;
            }

        } else {
            writeResponse(response, "Ha ocurrido un error inesperado al recoger los datos", -1);
        }
    }

    private Product getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println(payload);

        Product product = gson.fromJson(payload, Product.class);

        return product;
    }

    private void listByClub(HttpServletResponse response, String id) {
        try {
            int idc = Integer.parseInt(id);
            Club club = clubDao.findClubById(idc);
            if (club != null) {
                List<Product> list = productDao.findAllProductsInAClub(club);

                if (list != null) {
                    for (Product product : list) {
                        product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
                    }
                    writeResponse(response, list, -1);
                } else {
                    writeResponse(response, "Este club no tiene productos", -1);
                }
            } else {
                writeResponse(response, "Este club no existe", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id del club tiene que ser un campo válido", -1);
        }
    }

    private void findProductById(HttpServletResponse response, int id) {
        Product product = productDao.findProductById(id);

        if (product != null) {

            product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
            writeResponse(response, product, 1);
        } else {
            writeResponse(response, "Este producto no existe en la base de datos", -1);
        }
    }

    private void listByCategory(HttpServletResponse response, String paramether) {

        List<Product> list = productDao.findProductByCategory(paramether);

        if (list != null) {
            for (Product product : list) {
                product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
            }
            writeResponse(response, list, -1);
        } else {
            writeResponse(response, "No existe ningún producto con esta categoria", -1);
        }

    }

    private void listByPromotion(HttpServletResponse response, String id) {
        try {
            int ids = Integer.parseInt(id);
            Promotion promotion = promotionDao.findPromotionById(ids);
            if (promotion != null) {
                List<Product> list = productDao.findProductByPromotion(promotion);
                if (list != null) {
                    for (Product product : list) {
                        product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
                    }
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "No existen productos asociados con esta promoción", -1);
                }
            } else {
                writeResponse(response, "Esta promoción no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que tener un campo válido", -1);
        }
    }

    private void getProductsByAnyWhere(String param1, String param2, HttpServletResponse response) {

        switch (param1) {
            case "club":
                listByClub(response, param2);
                break;
            case "promotion":
                listByPromotion(response, param2);
                break;
            case "morethan":
                listByPrceMoreThan(response, param2);
                break;
            case "lessthan":
                listByPrceLessThan(response, param2);
                break;
            case "category":
                listByCategory(response, param2);
                break;
            default:
                writeResponse(response, "Opción inválida", -1);
                break;
        }
    }

    private void listByPrceMoreThan(HttpServletResponse response, String param) {
        try {
            double price = Double.parseDouble(param);
            List<Product> list = productDao.findProductMoreThan(price);

            if (list != null) {
                for (Product product : list) {
                    product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
                }
                writeResponse(response, list, 1);
            } else {
                writeResponse(response, "No hay productos con los precios mas altos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "Tiene que añadir un parámetro correcto para el precio", -1);
        }
    }

    private void listByPrceLessThan(HttpServletResponse response, String param) {
        try {
            double price = Double.parseDouble(param);
            List<Product> list = productDao.findProductLessThan(price);

            if (list != null) {
                for (Product product : list) {
                    product.setImageUrl(this.HOST_PATH + this.roothPath + product.getImageUrl());
                }
                writeResponse(response, list, 1);
            } else {
                writeResponse(response, "No hay productos con los precios mas bajos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "Tiene que añadir un parámetro correcto para el precio", -1);
        }
    }

    private void modifyProduct(Product product, HttpServletResponse response, HttpServletRequest request) {

        try {

            Product newProduct = getJsonValues(request);

            if (newProduct != null) {
                
                if(newProduct.getCategory() != null && newProduct.getCategory().length() > 0){
                    product.setCategory(newProduct.getCategory());
                }
                if(newProduct.getDescription() != null && newProduct.getDescription().length() > 0){
                    product.setDescription(newProduct.getDescription());
                }
                if(newProduct.getName() != null){
                    product.setName(newProduct.getName());
                }
                if(newProduct.getPrice() != -1){
                    product.setPrice(newProduct.getPrice());
                }
                
                if(newProduct.getImageUrl() != null && newProduct.getImageUrl().length() > 0){
                    utils.modifyImage(this.roothPhotoPath, product.getImageUrl(), newProduct.getImageUrl());
                }

                if (productDao.modifyProduct(product)) {
                    writeResponse(response, "El club se ha modificado con éxito", 1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de modificar el club", -1);
                }

            } else {
                writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos del club", -1);

            }

        } catch (IOException e) {
            writeResponse(response, "Ha ocurrido un error con el reader", -1);
        } catch (ServletException ex) {
            writeResponse(response, "Ha ocurrido un error con el servlet", -1);
        }
    }

}
