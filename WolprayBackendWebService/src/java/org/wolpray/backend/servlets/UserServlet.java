/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.pojo.User;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.persist.UserDao;


/**
 *
 * @author Lewis
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/userservlet/*"})
@MultipartConfig
public class UserServlet extends HttpServlet {

    private UserDao userDao;
    private Utils utils;

    @Override
    public void init() throws ServletException {

        try {
            DBConnect.loadDriver();
            userDao = new UserDao();
            
            utils = new Utils();
        } catch (ClassNotFoundException ex) {
            System.out.println("Error con la conexión de la base de datos");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            listAllUsers(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 2:
                    String paramether = splits[1];
                    try {
                        int id = Integer.parseInt(paramether);
                        listById(response, id);

                    } catch (NumberFormatException e) {
                        findUserByParamether(response, paramether);
                    }

                    break;
                default:
                    writeResponse(response, "Esta no es una opción válida", -1);
                    break;
            }

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Estoy entrando en post");
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {

            addUser(request, response);

        } else {
            writeResponse(response, "Acción inválida", -1);
        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        }

        String[] splits = pathInfo.split("/");

        if (splits.length != 2) {
            writeResponse(response, "Acción no válida", -1);
        }

        String id = splits[1];

        try {
            int ids = Integer.parseInt(id);
            User user = userDao.findUserById(ids);
            if (user != null) {
                if (userDao.removeUser(user)) {
                    writeResponse(response, "Se ha borrado el usuario correctamente", -1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de borrar el usuario", -1);
                }
            } else {
                writeResponse(response, "El usuario no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El campo id no es válido", -1);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    String id = splits[1];

                    try {
                        int ids = Integer.parseInt(id);

                        User user = userDao.findUserById(ids);
                        if (user != null) {
                            modifyUser(response, user, request);
                        } else {
                            writeResponse(response, "Este club no existe en a base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id ha de ser un campo válido. id: " + id, -1);
                    }
                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }

    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void listAllUsers(HttpServletResponse response) {
        List<User> list = userDao.findAllUsers();

        if (list != null) {

            writeResponse(response, list, 1);

        } else {
            writeResponse(response, "Ha ocurrido un error al tratar de buscar los clubs", 0);
        }
    }

    private void listById(HttpServletResponse response, int id) {

        User user = userDao.findUserById(id);

        if (user != null) {

            writeResponse(response, user, 1);
        } else {
            writeResponse(response, "Este usuario no existe en la base de datos", 0);
        }

    }

    private void findUserByParamether(HttpServletResponse response, String paramether) {

        List<User> list = userDao.findUserByRole(paramether);

        if (list != null && list.size() > 0) {
            writeResponse(response, list, 1);
        } else {
            User user = userDao.findUserByEmail(paramether);
            if (user != null) {
                writeResponse(response, user, 1);
            } else {
                user = userDao.findUserByPhone(paramether);
                if (user != null) {
                    writeResponse(response, user, 1);
                } else {
                    user = userDao.findUserByUserName(paramether);
                    if (user != null) {
                        writeResponse(response, user, 1);
                    } else {
                        writeResponse(response, "No existe este usuario en la base de datos", -1);
                    }
                }
            }
        }

    }

    private void addUser(HttpServletRequest request, HttpServletResponse response) {

        try {
            User user = getJsonValues(request);

            if (user != null) {
                int isAdded = validateUser(user);
                switch (isAdded) {
                    case -3:
                        writeResponse(response, "No puedes registrarte con este nombre de usuario porque ya está siendo usado", -3);
                        break;
                    case -2:
                        writeResponse(response, "No puedes registrarte con este correo electrónico porque ya está siendo usado", -2);
                        break;
                    case -1:
                        writeResponse(response, "No puedes registrarte con este teléfono porqe ya esta siendo usado", -1);
                        break;
                    case 0:
                        writeResponse(response, "Tienes que ser mayor de edad", 0);
                        break;
                    case 1:

                        writeResponse(response, "¡Te has registrado con éxito!", 1);

                        break;
                    default:
                        writeResponse(response, "Un error insperado ha ocurrido", -3);
                        break;
                }

            } else {
                writeResponse(response, "Un error insperado ha ocurrido", -5);
            }

        } catch (ServletException e) {
            writeResponse(response, "Servlet Exception: "+e.getMessage(), -5);

        } catch (IOException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int getYearsOld(String birthDate) {
        String split[] = birthDate.split("-");
        int yearInt = Integer.parseInt(split[0]);
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int yearsOld = currentYear - yearInt;
        return yearsOld;
    }

    private int validateUser(User user) {
        int result;
        User oldUser = userDao.findUserByUserName(user.getUserName());
        if (oldUser != null) {
            result = -3;
        } else {
            oldUser = userDao.findUserByEmail(user.getMail());
            if (oldUser != null) {
                result = -2;
            } else {
                oldUser = userDao.findUserByPhone(user.getPhone());
                if (oldUser != null) {
                    result = -1;
                } else {
                    int yearsOld = getYearsOld(user.getBirthDate());

                    if (yearsOld >= 18) {
                        result = userDao.addUser(user);
                    } else {
                        result = 0;
                    }
                }
            }
        }
        return result;
    }

    private void modifyUser(HttpServletResponse response, User user, HttpServletRequest request) {

        try {

            User newuser = getJsonValues(request);

            if (newuser != null) {

                if (newuser.getMail() != null && newuser.getMail().length()>0) {
                    user.setMail(newuser.getMail());
                }
                if (newuser.getPassword() != null && newuser.getPassword().length() > 0) {
                    user.setPassword(newuser.getPassword());
                }

                if (newuser.getPhone() != null && newuser.getPhone().length() > 0) {
                    user.setPhone(newuser.getPhone());
                }

                if (newuser.getRole() != null) {
                    user.setRole(newuser.getRole());
                }

                if (newuser.getUserName() != null && newuser.getUserName().length() > 0) {
                    user.setUserName(newuser.getUserName());
                }

                if (newuser.getBirthDate() != null && newuser.getBirthDate().length() > 0) {
                    user.setBirthDate(newuser.getBirthDate());
                }

                switch (userDao.modifyUser(user)) {
                    case -2:
                        writeResponse(response, "Este nobre de usuario ya existe en la base de datos", -1);
                        break;
                    case -1:
                        writeResponse(response, "Este email ya existe en la base de datos", -1);
                        break;
                    case 0:
                        writeResponse(response, "Este teléfono ya existe en la base de datos", -1);
                        break;
                    case 1:
                        writeResponse(response, "El usuario se ha modificado con éxito", 1);
                        break;
                    default:
                        writeResponse(response, "Ha ocurrido un error al tratar de modificar el usuario " + user.getUserName() + " en la base de datos", -1);
                        break;
                }
            } else {
                writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos del usuario", -1);
            }

        } catch (IOException e) {
            writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos del usuario", -1);
        } catch (ServletException ex) {
            writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos del usuario", -1);
        }
    }

    private User getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println("los datos: "+payload);

        User user = gson.fromJson(payload, User.class);

        return user;
    }

}
