/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.ClubDao;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.persist.ReservationDao;
import org.cat.proven.wolprayproject.models.persist.UserDao;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Reservation;
import org.cat.proven.wolprayproject.models.pojo.User;

/**
 *
 * @author Lewis
 */
@WebServlet(name = "ReservationServlet", urlPatterns = {"/reservationservlet/*"})
@MultipartConfig
public class ReservationServlet extends HttpServlet {

    private ReservationDao reservationDao;
    private ClubDao clubDao;
    private UserDao userDao;
    private Utils utils;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            clubDao = new ClubDao();
            reservationDao = new ReservationDao();
            userDao = new UserDao();
            utils = new Utils();
        } catch (ClassNotFoundException ex) {
            System.out.println("Conexión en la base de datos");
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            listAllRservations(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 3:
                    String param1 = splits[1];
                    String param2 = splits[2];
                    getReservationsByAnyWhere(param1, param2, response);
                    break;
                case 2:
                    String paramether = splits[1];
                    try {
                        int ids = Integer.parseInt(paramether);
                        findReservationById(response, ids);
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id tiene que ser un campo válido", 0);
                    }
                    break;
                default:
                    writeResponse(response, "Opción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción inválida", -1);

        } else {
            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 3:
                    try {
                    int clubid = Integer.parseInt(splits[1]);
                    int userid = Integer.parseInt(splits[2]);
                    Club club = clubDao.findClubById(clubid);
                    if (club != null) {
                        User user = userDao.findUserById(userid);
                        if (user != null) {
                            if ("client".equalsIgnoreCase(user.getRole())) {
                                addNewReservation(request, response, club, user);
                            } else {
                                writeResponse(response, "Este usuario no tiene permiso para realizar esta operación", -1);
                            }
                        } else {
                            writeResponse(response, "No existe ningún usuario con esta id", -1);
                        }

                    } else {
                        writeResponse(response, "No existe ningún club con esta id", -1);
                    }
                } catch (NumberFormatException e) {
                    writeResponse(response, "El id del club tiene que ser un campo válido", -1);
                }

                break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    try {
                        
                    int reservId = Integer.parseInt(splits[1]);

                    Reservation reservation = reservationDao.findReservationsById(reservId);

                    if (reservation != null) {
                        modifyReservation(request, response, reservation);
                    } else {
                        writeResponse(response, "Esta reserva no se encuentra en la base de datos", -1);
                    }

                } catch (NumberFormatException e) {
                    writeResponse(response, "El id de la reserva tiene que ser un campo válido", -1);
                }
                break;
                default:
                    writeResponse(response, "Esta no es una opción válida", -1);
                    break;
            }
        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        }

        String[] splits = pathInfo.split("/");

        if (splits.length != 2) {
            writeResponse(response, "Acción no válida", -1);
        }
        
        
        String id = splits[1];
        
        try {
            int ids = Integer.parseInt(id);
           
            Reservation reservation = reservationDao.findReservationsById(ids);
            if (reservation != null) {
                if (reservationDao.removeReservations(reservation)) {
                    writeResponse(response, "Se ha eliminado la reserva con éxito", -1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de borrar el club", -1);
                }
            } else {
                writeResponse(response, "El club no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El campo id no es válido", -1);
        }
        
    }
    
    
    

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void listAllRservations(HttpServletResponse response) {
        List<Reservation> list = reservationDao.findAllReservations();

        if (list != null && list.size() > 0) {
            writeResponse(response, list, 1);

        } else {
            writeResponse(response, "No hay reservas para mostrar", 0);
        }

    }

    private void getReservationsByAnyWhere(String param1, String param2, HttpServletResponse response) {
        switch (param1) {
            case "club":
                listByClub(response, param2);
                break;
            case "user":
                listByUser(response, param2);
                break;
            case "date":
                listByDate(response, param2);
                break;
            case "status":
                listByState(response, param2);
                break;
            default:
                writeResponse(response, "Acción inválida", -1);
                break;
        }
    }

    private void findReservationById(HttpServletResponse response, int id) {
        Reservation reservation = reservationDao.findReservationsById(id);
        if (reservation != null) {
            writeResponse(response, reservation, 1);
        } else {
            writeResponse(response, "Esta reserva no existe en la bse de datos", -1);
        }
    }

    private void listByClub(HttpServletResponse response, String param2) {
        try {
            int ids = Integer.parseInt(param2);
            Club club = clubDao.findClubById(ids);

            if (club != null) {
                List<Reservation> list = reservationDao.findReservationsByClub(club);

                if (list != null && list.size() > 0) {
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "Este club no tiene reservas disponibles", -1);
                }
            } else {
                writeResponse(response, "Este club no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id del club tiene que ser un campo válido", -1);
        }
    }

    private void listByUser(HttpServletResponse response, String param2) {
        try {
            int ids = Integer.parseInt(param2);
            User user = userDao.findUserById(ids);

            if (user != null) {
                List<Reservation> list = reservationDao.findReservationsByUser(user);

                if (list != null && list.size() > 0) {
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "Este club no tiene reservas disponibles", -1);
                }
            } else {
                writeResponse(response, "Este club no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id del club tiene que ser un campo válido", -1);
        }
    }

    private void listByDate(HttpServletResponse response, String param2) {

        List<Reservation> list = reservationDao.findReservationsByDate(param2);

        if (list != null && list.size() > 0) {
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "Este club no tiene reservas disponibles", -1);
        }

    }

    private void listByState(HttpServletResponse response, String param2) {

        List<Reservation> list = reservationDao.findReservationsByState(param2);

        if (list != null && list.size() > 0) {
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "Este club no tiene reservas disponibles", -1);
        }
    }

    private Reservation getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println(payload);

        Reservation reservation = gson.fromJson(payload, Reservation.class);

        return reservation;
    }

    private void addNewReservation(HttpServletRequest request, HttpServletResponse response, Club club, User user) {
        
        
        
        try {
            Reservation reservation = getJsonValues(request);
            if (reservation != null) {
            try {
                switch (validateReservation(reservation.getDate())) {
                    case 1:
                        confirmReservation(response, reservation);
                        break;
                    case -2:
                        writeResponse(response, "Para realizar la reserva tiene que ser una fecha posterior a la actual", -1);
                        break;
                    case -1:
                        writeResponse(response, "La fecha no tiene un formato correcto", -1);
                        break;
                }
            } catch (NumberFormatException e) {
                writeResponse(response, "El numero de personas tiene que ser un campo válido", -1);
            }

        }
            
        } catch (IOException ex) {
            writeResponse(response, "Ha ocurrido un problema al tratar de recoger los datos", -1);
        } catch (ServletException ex) {
            writeResponse(response, "Ha ocurrido un error con los servlets", -1);
        }

        

    }

    private int validateReservation(String date) {
        int result;

        String regexp = "^\\d{4}([\\-/.])(0?[1-9]|1[1-2])\\1(3[01]|[12][0-9]|0?[1-9])$";

        if (date.matches(regexp)) {
            String[] dateSplited = date.split("-");

            int year = Integer.parseInt(dateSplited[0]);
            int month = Integer.parseInt(dateSplited[1]);
            int day = Integer.parseInt(dateSplited[2]);

            Calendar calendar = Calendar.getInstance();

            int currentYear = calendar.get(Calendar.YEAR);
            int currentMonth = calendar.get(Calendar.MONTH) + 1;
            int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
            if ((year >= currentYear) && (month >= currentMonth) && (day >= currentDay) && (month >= currentMonth) && (day >= currentDay)) {
                result = 1;
            } else {
                result = -1;
            }

        } else {
            result = -2;

        }

        return result;
    }

    private void confirmReservation(HttpServletResponse response, Reservation reservation) {
        if (reservationDao.addReservation(reservation)) {
            writeResponse(response, "La reserva se ha realizado correctamente", 1);
        } else {
            writeResponse(response, "Ha ocurrido un error al tratar de realizar la reserva en la base de datos", -1);
        }
    }

    private void modifyReservation( HttpServletRequest request, HttpServletResponse response, Reservation reservation) {


        try {
            Reservation newValues = getJsonValues(request);
            
            if(newValues != null){
                if(newValues.getStatus() != null && newValues.getStatus().equalsIgnoreCase("Aceptada")){
                    reservation.setStatus("Aceptada");
                }else if(newValues.getStatus() != null && newValues.getStatus().equalsIgnoreCase("Rechazada")){
                    reservation.setStatus("Rechazada");
                }else if(newValues.getStatus() != null && newValues.getStatus().equalsIgnoreCase("En espera")){
                    reservation.setStatus("En espera");
                }
                
                if(newValues.getNotes() != null && newValues.getNotes().length() >0){
                    reservation.setNotes(newValues.getNotes());
                }
                
                if(newValues.getDate() != null && newValues.getDate().length() >0){
                    reservation.setDate(newValues.getDate());
                }
                
                if(newValues.getN_people() != -1){
                    reservation.setN_people(newValues.getN_people());
                }
                
                if(reservationDao.modifyReservation(reservation)){
                    writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos", -1);
                }else{
                    
                }
            }else{
                writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos", -1);
            }
        } catch (IOException ex) {
            writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos", -1);
        } catch (ServletException ex) {
            writeResponse(response, "Ha ocurrido un error con los servlets", -1);
        }
            
    }
}
