/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.ClubDao;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.persist.OrderDao;
import org.cat.proven.wolprayproject.models.persist.ProductDao;
import org.cat.proven.wolprayproject.models.persist.PromotionDao;
import org.cat.proven.wolprayproject.models.persist.UserDao;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Order;
import org.cat.proven.wolprayproject.models.pojo.Product;
import org.cat.proven.wolprayproject.models.pojo.Promotion;
import org.cat.proven.wolprayproject.models.pojo.User;

/**
 *
 * @author Lewis
 */
@WebServlet(name = "OrdersServlet", urlPatterns = {"/orderservlet/*"})
@MultipartConfig
public class OrdersServlet extends HttpServlet {

    private Utils utils;
    private OrderDao orderDao;
    private ClubDao clubDao;
    private PromotionDao promotionDao;
    private UserDao userDao;
    private ProductDao productDao;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            clubDao = new ClubDao();
            orderDao = new OrderDao();
            promotionDao = new PromotionDao();
            utils = new Utils();
            userDao = new UserDao();
            productDao = new ProductDao();
        } catch (ClassNotFoundException ex) {
            System.out.println("Fallo en la conexión a la base de datos");
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            listAllOrders(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 2:
                    String paramether = splits[1];
                    listOrderById(response, paramether);
                    break;
                case 3:
                    String param1 = splits[1];
                    String param2 = splits[2];
                    listByParam(param1, param2, response);
                    break;
                default:
                    writeResponse(response, "Opción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);

        } else {
            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 4:
                    String clubIdss = splits[1];
                    String userIDss = splits[2];
                    String productIdss = splits[3];
                    try {
                        long clubId = Long.parseLong(clubIdss);
                        long productId = Long.parseLong(productIdss);
                        int userID = Integer.parseInt(userIDss);
                        addOrderWithNoPromotion(clubId, productId, userID, response);
                    } catch (NumberFormatException e) {
                        writeResponse(response, "Los ids tienen que ser campos válidos", -1);
                    }
                    break;
                case 5:
                    String clubIds = splits[1];
                    String userIDs = splits[2];
                    String productIds = splits[3];
                    String promotionIds = splits[4];
                    try {
                        long clubId = Long.parseLong(clubIds);
                        long productId = Long.parseLong(productIds);
                        long promotionId = Long.parseLong(promotionIds);
                        int userID = Integer.parseInt(userIDs);
                        addOrder(clubId, productId, promotionId, userID, response);
                    } catch (NumberFormatException e) {
                        writeResponse(response, "Los ids tienen que ser campos válidos", -1);
                    }
                    break;
                default:
                    writeResponse(response, "Faltan parámetros", -1);
                    break;
            }
        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        } else {
            String[] splits = pathInfo.split("/");

            if (splits.length != 2) {
                writeResponse(response, "Acción no válida", -1);
            }

            String id = splits[1];

            try {
                int ids = Integer.parseInt(id);
                Order order = orderDao.findById(ids);
                if (order != null) {
                    if (orderDao.deleteOrder(order)) {
                        writeResponse(response, "La orden se ha eliminado correctamente", 1);
                    } else {
                        writeResponse(response, "Ha ocurrido un error al tratar de borrar la orden", -1);
                    }
                } else {
                    writeResponse(response, "Esta orden no existe en la base de datos", -1);
                }
            } catch (Exception e) {
                writeResponse(response, "El campo id no es válido", -1);
            }
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    String id = splits[1];

                    try {
                        int ids = Integer.parseInt(id);

                        Order order = orderDao.findById(ids);

                        if (order != null) {
                            changeOrder(response, order, request);
                        } else {
                            writeResponse(response, "Este club no existe en a base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id ha de ser un campo válido. id: " + id, -1);
                    }
                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }
        }
    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void listAllOrders(HttpServletResponse response) {

        List<Order> list = orderDao.findAllOrders();
        if (list != null && list.size() > 0) {
            writeResponse(response, list, 1);
        } else {
            writeResponse(response, "No hay ordenes para mostrar", 1);
        }

    }

    private void listByParam(String param1, String param2, HttpServletResponse response) {
        switch (param1) {
            case "club":
                listOrdersByClub(param2, response);
                break;
            case "promotion":
                listOrdersByPromotion(param2, response);
                break;
            case "user":
                listOrdersByUsers(param2, response);
                break;
            case "product":
                listOrdersByProduct(param2, response);
                break;
            case "status":
                listOrdersByStatus(param2, response);
                break;
            default:
                writeResponse(response, "Acción inválida", -1);
                break;

        }
    }

    private void listOrderById(HttpServletResponse response, String param) {
        try {
            int ids = Integer.parseInt(param);
            Order order = orderDao.findById(ids);
            if (order != null) {
                writeResponse(response, order, 1);
            } else {
                writeResponse(response, "No hay órdenes con este id", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }
    }

    private void listOrdersByClub(String id, HttpServletResponse response) {
        try {
            long ids = Long.parseLong(id);
            Club club = clubDao.findClubById(ids);

            if (club != null) {
                List<Order> list = orderDao.findByClub(club);
                if (list != null && list.size() > 0) {
                    writeResponse(response, list, -1);
                } else {
                    writeResponse(response, "Este club no tiene ordenes actualmente", -1);
                }
            } else {
                writeResponse(response, "Este club no existe en la base de datos", -1);
            }

        } catch (Exception e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }

    }

    private void listOrdersByPromotion(String id, HttpServletResponse response) {

        try {
            long ids = Long.parseLong(id);

            Promotion promotion = promotionDao.findPromotionById(ids);
            if (promotion != null) {
                List<Order> list = orderDao.findByPromotion(promotion);
                if (list != null && list.size() > 0) {
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "No hay ninguna orden asociada con esta promoción", -1);
                }
            } else {
                writeResponse(response, "Esta promoción no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }

    }

    private void listOrdersByUsers(String id, HttpServletResponse response) {

        try {
            int ids = Integer.parseInt(id);

            User user = userDao.findUserById(ids);
            if (user != null) {
                List<Order> list = orderDao.findByUser(user);
                if (list != null && list.size() > 0) {
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "No existe ninguna orden asociada con este usuari", -1);
                }
            } else {
                writeResponse(response, "Este usuario no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }
    }

    private void listOrdersByProduct(String id, HttpServletResponse response) {

        try {
            long ids = Long.parseLong(id);

            Product product = productDao.findProductById(ids);
            if (product != null) {
                List<Order> list = orderDao.findByProduct(product);
                if (list != null && list.size() > 0) {
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "No existe ninguna orden asociada con este producto", -1);
                }
            } else {
                writeResponse(response, "Este producto no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }
    }

    private void listOrdersByStatus(String status, HttpServletResponse response) {

        List<Order> list = orderDao.findByStatus(status);
        if (list != null && list.size() > 0) {
            writeResponse(response, list, -1);
        } else {
            writeResponse(response, "Esta orden no existe en la base de datos", -1);
        }
    }

    private void addOrder(long clubId, long productId, long promotionId, int userID, HttpServletResponse response) {

        Club club = clubDao.findClubById(clubId);
        if (club != null) {
            Product product = productDao.findProductById(productId);
            if (product != null) {
                User user = userDao.findUserById(userID);
                if (user != null) {
                    Promotion promotion = promotionDao.findPromotionById(promotionId);
                    if (promotion != null) {
                        double price = productDao.getPrice(product, promotion);
                        Order order = new Order(club.getId(), user.getId(), product.getId(), promotion.getId(), price, "No pagado");

                        if (orderDao.addOrder(order)) {
                            writeResponse(response, "Esta promoción no existe en la base de datos", 1);
                        } else {
                            writeResponse(response, "Ha ocurrido un error al intentar añadir la orden en la base de datos", -1);
                        }

                    } else {
                        writeResponse(response, "Esta promoción no existe en la base de datos", -1);
                    }
                } else {
                    writeResponse(response, "Este usuario no existe en la base de datos", -1);
                }
            } else {
                writeResponse(response, "Este producto no existe en la base de datos", -1);
            }
        } else {
            writeResponse(response, "Este club no existe en la base de datos", -1);
        }
    }

    private void addOrderWithNoPromotion(long clubId, long productId, int userID, HttpServletResponse response) {

        Club club = clubDao.findClubById(clubId);
        if (club != null) {
            Product product = productDao.findProductById(productId);
            if (product != null) {
                User user = userDao.findUserById(userID);
                if (user != null) {

                    Order order = new Order(club.getId(), user.getId(), product.getId(), product.getPrice(), "No pagado");

                    if (orderDao.addOrderWithNoPromotion(order)) {
                        writeResponse(response, "Esta promoción no existe en la base de datos", 1);
                    } else {
                        writeResponse(response, "Ha ocurrido un error al intentar añadir la orden en la base de datos", -1);
                    }

                } else {
                    writeResponse(response, "Este usuario no existe en la base de datos", -1);
                }
            } else {
                writeResponse(response, "Este producto no existe en la base de datos", -1);
            }
        } else {
            writeResponse(response, "Este club no existe en la base de datos", -1);
        }
    }

    private void changeOrder(HttpServletResponse response, Order order, HttpServletRequest request) {

        try {

            Order values = getJsonValues(request);

            if (values != null) {
                if(values.getStatus() != null && values.getStatus().length() > 0)
                    order.setStatus(values.getStatus());
                if (orderDao.changeOrder(order)) {
                    writeResponse(response, "La orden se ha modificado con éxito", 1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de modificar la orden", -1);
                }

            } else {
                writeResponse(response, "Ha ocurrido un error al tratar de obtener los datos de la orden", -1);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el reader", -1);
        } catch (ServletException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el servlet", -1);
        }
    }

    private Order getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println(payload);

        Order order = gson.fromJson(payload, Order.class);

        return order;
    }
}
