/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.cat.proven.wolprayproject.models.persist.ClubDao;
import org.cat.proven.wolprayproject.models.persist.DBConnect;
import org.cat.proven.wolprayproject.models.persist.PromotionDao;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Promotion;

/**
 *
 * @author Lewis
 */
@WebServlet(name = "PromotionServlet", urlPatterns = {"/promotionservlet/*"})
public class PromotionServlet extends HttpServlet {

    private PromotionDao promotionDao;
    private ClubDao clubDao;
    private Utils utils;
    private final String HOST_PATH = "http://46.101.236.229:8080";
    private String roothPath;
    private String roothPhotoPath;

    @Override
    public void init() throws ServletException {
        try {
            DBConnect.loadDriver();
            clubDao = new ClubDao();
            promotionDao = new PromotionDao();
            utils = new Utils();
            this.roothPath = getServletContext().getContextPath();
            this.roothPhotoPath = getServletContext().getRealPath("/") + "assets";
        } catch (ClassNotFoundException ex) {
            System.out.println("Fallo en la conexión a la base de datos");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            findAllPromotions(response);

        } else {

            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 3:
                    String club = splits[1];
                    String id = splits[2];
                    if (club.equalsIgnoreCase("club")) {
                        findPromotionsByClub(response, id);
                    } else {
                        writeResponse(response, "Opción inválida", -1);
                    }

                    break;
                case 2:
                    String paramether = splits[1];
                    try {
                        int ids = Integer.parseInt(paramether);
                        Promotion promotion = promotionDao.findPromotionById(ids);
                        if (promotion != null) {

                            promotion.setCoverUrl(this.HOST_PATH + this.roothPath + promotion.getCoverUrl());

                            writeResponse(response, promotion, -1);
                        } else {
                            writeResponse(response, "Esta promoción no existe en a base de datos", -1);
                        }

                    } catch (Exception e) {
                        writeResponse(response, "El id tiene que tener un campo válido", -1);
                    }
                    break;
                default:
                    writeResponse(response, "Opción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción inválida", -1);

        } else {
            String[] splits = pathInfo.split("/");

            switch (splits.length) {
                case 2:
                    String clubid = splits[1];
                    try {
                        int clubids = Integer.parseInt(clubid);

                        Club club = clubDao.findClubById(clubids);
                        if (club != null) {
                            addPromotion(request, response, club);
                        } else {
                            writeResponse(response, "Este club no existe en la base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id del club tiene que ser un campo válido", -1);
                    }

                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {

            writeResponse(response, "Acción inválida", -1);
        } else {
            String[] splits = pathInfo.split("/");
            switch (splits.length) {
                case 2:
                    String id = splits[1];

                    try {
                        int ids = Integer.parseInt(id);

                        Promotion promotion = promotionDao.findPromotionById(ids);
                        if (promotion != null) {
                            modifyPromotion(response, promotion, request);
                          
                        } else {
                            writeResponse(response, "Este club no existe en a base de datos", -1);
                        }
                    } catch (NumberFormatException e) {
                        writeResponse(response, "El id ha de ser un campo válido. id: " + id, -1);
                    }
                    break;
                default:
                    writeResponse(response, "Acción inválida", -1);
                    break;
            }

        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            writeResponse(response, "Acción no válida", -1);
        }

        String[] splits = pathInfo.split("/");

        if (splits.length != 2) {
            writeResponse(response, "Acción no válida", -1);
        }

        String id = splits[1];

        try {
            long ids = Long.parseLong(id);
            Promotion promotion = promotionDao.findPromotionById(ids);
            if (promotion != null) {
                if (promotionDao.deletePromotion(promotion)) {
                    if (utils.deleteFile(getServletContext().getRealPath("/") + promotion.getCoverUrl())) {
                        writeResponse(response, "Se ha eliminado la promoción con éxito", -1);

                    } else {
                        writeResponse(response, "Ha ocurrido un error al tratar de borrar la foto de la promoción", -1);
                    }
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de borrar la promoción", -1);
                }
            } else {
                writeResponse(response, "La promoción no existe en la base de datos", -1);
            }
        } catch (Exception e) {
            writeResponse(response, "El campo id no es válido", -1);
        }
    }

    private void findAllPromotions(HttpServletResponse response) {

        List<Promotion> list = promotionDao.findAllPromotions();

        if (list != null) {
            for (Promotion promotion : list) {
                promotion.setCoverUrl(this.HOST_PATH +this.roothPath + promotion.getCoverUrl());
            }

            writeResponse(response, list, 1);

        } else {
            writeResponse(response, "Ha ocurrido un error al tratar de buscar las promociones en la base de datos", -1);
        }
    }

    private void writeResponse(HttpServletResponse response, Object data, int code) {

        utils.writeRequest(response, data, code);
    }

    private void findPromotionsByClub(HttpServletResponse response, String id) {
        try {
            int ids = Integer.parseInt(id);
            Club club = clubDao.findClubById(ids);

            if (club != null) {
                List<Promotion> list = promotionDao.findPromotionsByClub(club);
                if (list != null && list.size() > 0) {
                    for (Promotion promotion : list) {
                        promotion.setCoverUrl(this.HOST_PATH + this.roothPath + promotion.getCoverUrl());
                    }
                    writeResponse(response, list, 1);
                } else {
                    writeResponse(response, "Este club no tiene promociones disponibles", -1);
                }
            } else {
                writeResponse(response, "Este club no existe en la base de datos", -1);
            }
        } catch (NumberFormatException e) {
            writeResponse(response, "El id tiene que ser un campo válido", -1);
        }
    }

    private void addPromotion(HttpServletRequest request, HttpServletResponse response, Club club) throws IOException, ServletException {
        Promotion promotion = getJsonValues(request);

        if (promotion != null) {

            int promotionId = promotionDao.getPromitionId();
            promotion.setId(promotionId);

            int result = validatePromotion(promotion, club);

            switch (result) {
                case 1:
                    writeResponse(response, "La promoción se ha agregado correctamente", 1);
                    break;
            }
        } else {
            writeResponse(response, "Falta uno de los campos obligatorios", -1);
        }
    }

    private int validatePromotion(Promotion promotion, Club club) {
        int result;

        String image64 = promotion.getCoverUrl();
        if (promotionDao.findPromotionById(promotion.getId()) != null) {
            long id = promotion.getId() + 1;
            promotion.setId(id);
            promotion.setCoverUrl("/assets/promotions/"+promotion.getId() + ".jpg");
        }
        if (promotionDao.addNewPromotion(club, promotion)) {
           if (utils.createImageFromBase64(image64, this.roothPhotoPath, "promotions", promotion.getId() + ".jpg")) {
                    result = 1;
                } else {
                    promotionDao.deletePromotion(promotion);
                    result = -1;
                }
        } else {
            result = 0;
        }

        return result;
    }
    

    private void modifyPromotion(HttpServletResponse response, Promotion promotion, HttpServletRequest request) {

        try {

            Promotion values = getJsonValues(request);

            if (values != null) {
                if (values.getDescription() != null && values.getDescription().length() > 0) {
                    promotion.setDescription(values.getDescription());
                }

                if (values.getName() != null && values.getName().length() > 0) {
                    promotion.setDescription(values.getName());
                }
                
                if(values.getCoverUrl() != null && values.getCoverUrl().length() > 0){
                    utils.modifyImage(this.roothPhotoPath, promotion.getCoverUrl(), values.getCoverUrl());
                }

                if (promotionDao.modifyPromotion(promotion)) {
                    writeResponse(response, "La promoción se ha modificado con éxito", 1);
                } else {
                    writeResponse(response, "Ha ocurrido un error al tratar de modificar la promoción", -1);
                }
            } else {
                writeResponse(response, "Ha ocurrido un error al recoger los datos de la promocion", -1);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el reader", -1);
        } catch (ServletException ex) {
            ex.printStackTrace();
            writeResponse(response, "Ha ocurrido un error con el servlet", -1);
        }
    }

    private Promotion getJsonValues(HttpServletRequest request) throws IOException, ServletException {
        Gson gson = new Gson();

        System.out.println("Voy a recoger el json");
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = request.getReader();

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            buffer.append(line);
        }

        String payload = buffer.toString();
        System.out.println(payload);

        Promotion promotion = gson.fromJson(payload, Promotion.class);

        return promotion;
    }
}
