package org.wolpray.backend.servlets;

import com.google.gson.Gson;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lewis
 */
public class Utils {

    private String pathForBDd;

    public Utils() {
    }

    public boolean createImageFromBase64(String base64String, String rootPath, String finalPath, String fileName) {
        boolean result = false;
        
        this.pathForBDd = "";
        String[] strings = base64String.split(",");

        File file = getFile(rootPath, finalPath, fileName);
        if (file != null) {
            BufferedImage image = decodeToImage(strings[1]);
            try {
                String extension;
                switch (strings[0]) {//check image's extension
                    case "data:image/jpeg;base64":
                        extension = "jpeg";
                        break;
                    case "data:image/png;base64":
                        extension = "png";
                        break;
                    default://should write cases for more images types
                        System.out.println("Entre en el default");
                        extension = "jpg";
                        break;
                }
                ImageIO.write(image, extension, file);
                result = true;
                System.out.println("La foto está en: " + file.getAbsolutePath());
                this.pathForBDd = "/assets/" + finalPath + "/" + fileName;
                System.out.println("El path final es: "+pathForBDd);
            } catch (IOException e) {
                System.out.println("Ha saltado la excepción:");
                e.printStackTrace();
                result = false;
            }

        }

        return result;
    }

    public boolean modifyImage(String contextPath, String imageUrl, String base64Image) {
        boolean result = false;
        String finalPath = contextPath + File.separator + imageUrl;

        if (deleteFile(finalPath)) {
            String[] directories = imageUrl.split("/");
            String rothPath = contextPath + directories[1];
            String finalDirectory = directories[2];
            String fileName = directories[3];

            result = createImageFromBase64(base64Image, rothPath, finalDirectory, fileName);
        }

        return result;
    }

    public boolean deleteFile(String path) {
        boolean isDeleted = false;
        File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            isDeleted = file.delete();
        }

        return isDeleted;
    }

    public static BufferedImage decodeToImage(String imageString) {
        BufferedImage image = null;
        byte[] imageByte;
        try {
            imageByte = Base64.getDecoder().decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    private File getFile(String rootPath, String finalPath, String fileName) {
        File imageFile = null;

        File rootDirectory = getRootDirectory(rootPath);

        if (rootDirectory != null) {
            File finalDirectory = getFinalDirectory(rootDirectory.getAbsolutePath() + File.separator + finalPath);
            if (finalDirectory != null) {

                imageFile = new File(finalDirectory.getAbsolutePath() + File.separator + fileName);
                System.out.println("Su path es " + imageFile.getAbsolutePath());
            }
        }
        return imageFile;
    }

    private File getRootDirectory(String rootPath) {
        File rootDirectory = new File(rootPath);
        if (rootDirectory.exists() && rootDirectory.isDirectory()) {
            return rootDirectory;
        } else {
            if (rootDirectory.mkdirs()) {
                return rootDirectory;
            } else {
                return null;
            }
        }
    }

    private File getFinalDirectory(String finalPath) {
        File finalDirectory = new File(finalPath);
        if (finalDirectory.exists() && finalDirectory.isDirectory()) {
            return finalDirectory;
        } else {

            if (finalDirectory.mkdirs()) {

                return finalDirectory;
            } else {

                return null;
            }
        }
    }

    public void writeRequest(HttpServletResponse response, Object data, int code) {
        try {
            RequestResult result = new RequestResult(data, code);
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD");
            response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
            response.getWriter().write(new Gson().toJson(result));
            response.flushBuffer();
        } catch (IOException e) {
        }
    }

    public String getPathForBDd() {
        return pathForBDd;
    }

}
