/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cat.proven.wolprayproject.models.pojo;

import java.util.List;
import java.util.Map;



/**
 *
 * @author Lewis
 */
public class Club {
    //Attributes
    private long id;
    private String name;
    private String streetName;
    private String streetNumber;
    private String postalCode;
    private String city;
    private String description;
    private String ambience;
    private String phone;
    private String dressCode;
    private String coverUrl;
    private String latitude;
    private String longitude;
    List<Slot> schedule;
    private Availability availability; 
    
    
    
    
    //Constructors

    public Club(long id, String name, String streetName, String streetNumber, String 
            postalCode, String city, String description, String ambience, String phone, 
            String dressCode, String coverUrl, String latitude, String longitude, List<Slot> schedule, 
            Availability availability) {
        this.id = id;
        this.name = name;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.description = description;
        this.ambience = ambience;
        this.phone = phone;
        this.dressCode = dressCode;
        this.coverUrl = coverUrl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.schedule = schedule;
        this.availability = availability;
    }
    
    public Club(long id, String name, String streetName, String streetNumber, String 
            postalCode, String city, String description, String ambience, String phone, 
            String dressCode, String coverUrl, String latitude, String longitude) {
        this.id = id;
        this.name = name;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.description = description;
        this.ambience = ambience;
        this.phone = phone;
        this.dressCode = dressCode;
        this.coverUrl = coverUrl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.schedule = schedule;
        this.availability = availability;
    }

    
    
   
 

    public Club(int id) {
        this.id = id;
    }
    
    public Club() {
    }

    
    //Accesors
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    
    

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String City) {
        this.city = City;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmbience() {
        return ambience;
    }

    public void setAmbience(String ambience) {
        this.ambience = ambience;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDressCode() {
        return dressCode;
    }

    public void setDressCode(String dressCode) {
        this.dressCode = dressCode;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public List<Slot> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<Slot> schedule) {
        this.schedule = schedule;
    }
    
    
    /** equals()
	 * compares this club to another one
	 * two persons are equals if their nifs are equals.
	 * @param obj other: the other club to compare to
	 * @return true if they are equals, false otherwise
	 */
    @Override
    public boolean equals(Object obj) {
		boolean b = false;
		if (obj == null) {
			b= false;
		} else {
			if (obj == this) {
				b = true;
			} else {
				if (obj instanceof Club) {
				    Club other = (Club) obj;
				    b = (this.id == other.id);
				} else {
					b = false;
				}
			}
		}
		return b;
	}

    @Override
    public String toString() {
        return "Club{" + "id=" + id + ", name=" + name + ", streetName=" + streetName + ", streetNumber=" + streetNumber + ", postalCode=" + postalCode + ", City=" + city + ", description=" + description + ", ambience=" + ambience + ", phone=" + phone + ", dressCode=" + dressCode + ", productList=" + ", schedule=" + schedule + '}';
    }
}
