/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cat.proven.wolprayproject.models.pojo;

import java.sql.Date;
import java.util.Map;

/**
 *
 * @author Lewis
 */
public class Reservation {
    private int reservId;
    private long clubId;
    private int userId;
    private int n_people = -1;
    private String notes;
    private String date;
    private String status;

    public Reservation(int reservId, long clubId, int userId, int n_people, String notes, String date, String status) {
        this.reservId = reservId;
        this.clubId = clubId;
        this.userId = userId;
        this.n_people = n_people;
        this.notes = notes;
        this.date = date;
        this.status = status;
    }
    
    public Reservation( long clubId, int userId, int n_people, String notes, String date, String status) {
        this.clubId = clubId;
        this.userId = userId;
        this.n_people = n_people;
        this.notes = notes;
        this.date = date;
        this.status = status;
    }

 

    public long getClubId() {
        return clubId;
    }

    public void setClunId(int clunId) {
        this.clubId = clunId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getN_people() {
        return n_people;
    }

    public void setN_people(int n_people) {
        this.n_people = n_people;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    

    public String getStatus() {
        return status;
    }

    public void setStatus(String state) {
        this.status = state;
    }

    public int getReservId() {
        return reservId;
    }

    public void setReservId(int reservId) {
        this.reservId = reservId;
    }

    @Override
    public String toString() {
        return "Reservation{" + "reservId=" + reservId + ", clubId=" + clubId + ", userId=" + userId + ", n_people=" + n_people + ", notes=" + notes + ", date=" + date + ", status=" + status + '}';
    }
    
    

    
}
