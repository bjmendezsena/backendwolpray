/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cat.proven.wolprayproject.models.pojo;

/**
 *
 * @author Lewis
 */
public class Availability {
    private boolean isOpen;
    private boolean reservationAvailable;
    private boolean itsFullCapacity;

    public Availability( boolean isOpen, boolean reservationAvailable, boolean itsFullCapacity) {
        this.isOpen = isOpen;
        this.reservationAvailable = reservationAvailable;
        this.itsFullCapacity = itsFullCapacity;
    }


    public boolean isIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public boolean isReservationAvailable() {
        return reservationAvailable;
    }

    public void setReservationAvailable(boolean reservationAvailable) {
        this.reservationAvailable = reservationAvailable;
    }

    public boolean isItsFullCapacity() {
        return itsFullCapacity;
    }

    public void setItsFullCapacity(boolean itsFullCapacity) {
        this.itsFullCapacity = itsFullCapacity;
    }

    @Override
    public String toString() {
        return "Availability{" + ", isOpen=" + isOpen + ", reservationAvailable=" + reservationAvailable + ", itsFullCapacity=" + itsFullCapacity + '}';
    }
}
