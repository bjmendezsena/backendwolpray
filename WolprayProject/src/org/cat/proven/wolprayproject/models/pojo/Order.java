/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cat.proven.wolprayproject.models.pojo;

/**
 *
 * @author Lewis
 */
public class Order {
    private int orderId;
    private long clubId;
    private int userId;
    private long productId;
    private long promotionId;
    private double price;
    private String status;

    public Order(int orderId, long clubId, int userId, long productId, long promotionId, double price, String status) {
        this.orderId = orderId;
        this.clubId = clubId;
        this.userId = userId;
        this.productId = productId;
        this.promotionId = promotionId;
        this.price = price;
        this.status = status;
    }
    
    public Order( long clubId, int userId, long productId, double price, String status) {
        this.clubId = clubId;
        this.userId = userId;
        this.productId = productId;
        this.price = price;
        this.status = status;
    }
    
    public Order(long clubId, int userId, long productId, long promotionId, double price, String status) {
        this.clubId = clubId;
        this.userId = userId;
        this.productId = productId;
        this.promotionId = promotionId;
        this.price = price;
        this.status = status;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public long getClubId() {
        return clubId;
    }

    public void setClubId(long clubId) {
        this.clubId = clubId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(long promotionId) {
        this.promotionId = promotionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
