package org.cat.proven.wolprayproject.models.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Product;
import org.cat.proven.wolprayproject.models.pojo.Promotes;
import org.cat.proven.wolprayproject.models.pojo.Promotion;

/**
 * Manages access to database data.
 *
 * @author Lewis
 */
public class ProductDao {

    private Connection conn;

    /**
     * Empty builder.
     */
    public ProductDao() {
    }

    public List<Product> findAllProductsInAClub(Club club) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_ALL_PRODUCTS_IN_CLUB);
                ps.setLong(1, club.getId());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public List<Product> findAllProducts() {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_ALL_PRODUCTS);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public List<Product> findProductByName(String name) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PRODUCT_WHERE_NAME);
                ps.setString(1, name.toLowerCase());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public List<Product> findProductByPromotion(Promotion promotion) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            System.out.println(conn);
            PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PROMOTES_BY_PROMOTION);
            ps.setLong(1, promotion.getId());
            ResultSet rs = ps.getResultSet();

            while (rs.next()) {
                Product product = getProduct(rs);
                if (product != null) {
                    result.add(product);
                }

            }

        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public Product findProductById(long id) {
        Product product = null;
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PRODUCT_WHERE_ID);
                ps.setLong(1, id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    product = getProduct(rs);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            product = null;
        }
        return product;
    }

    public boolean deleteProduct(Product product) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ProductQueries.DELETE_PRODUCT_WHERE_ID);
            ps.setLong(1, product.getId());
            if (ps.executeUpdate() > 0) {
                result = true;
            }

        } catch (SQLException ex) {
            result = false;
        }
        return result;
    }

    public List<Product> findProductByCategory(String category) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PRODUCT_WHERE_CATEGORY);
                ps.setString(1, category);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public List<Product> findProductMoreThan(double price) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PRODUCT_WHERE_MORE_THAN);
                ps.setDouble(1, price);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public List<Product> findProductLessThan(double price) {
        List<Product> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.SELECT_PRODUCT_WHERE_LESS_THAN);
                ps.setDouble(1, price);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Product product = getProduct(rs);
                    result.add(product);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    public boolean addProduct(Product product) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.INSERT_NEW_PRODUCT);
                ps.setLong(1, product.getId());
                ps.setLong(2, product.getClubId());
                ps.setString(3, product.getName());
                ps.setString(4, product.getDescription());
                ps.setDouble(5, product.getPrice());
                ps.setString(6, product.getImageUrl());
                ps.setString(7, product.getCategory());
                ps.setInt(8, 1);
                if (ps.executeUpdate() > 0) {
                    result = true;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            result = false;
        }
        return result;
    }

    private Product getProduct(ResultSet rs) {
        Product product;
        try {
            long id = rs.getLong("productid");
            String name = rs.getString("productname");
            String dscription = rs.getString("description");
            double price = rs.getDouble("price");
            String category = rs.getString("category");
            String imageUrl = rs.getString("image_url");
            boolean isInStock;
            int status = rs.getInt("status");
            if (status == 0) {
                isInStock = false;
            } else {
                isInStock = true;
            }

            product = new Product(id, name, category, dscription, price, imageUrl, isInStock);

        } catch (SQLException e) {
            product = null;
        }

        return product;
    }

    public int getLastProductId() {
        int result = -1;
        String query = "SELECT max(productid) as id FROM products";

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getInt("id");
            }else{
                result = 1;
            }
        } catch (SQLException ex) {
            result = -1;
        }

        return result;
    }

    public boolean modifyProduct(Product product) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(ProductQueries.UPDATE_PRODUCT);
                ps.setString(1, product.getName());
                ps.setString(2, product.getDescription());
                ps.setDouble(3, product.getPrice());
                ps.setString(4, product.getCategory());
                ps.setLong(5, product.getId());
                if (ps.executeUpdate() > 0) {
                    result = true;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            result = false;
        }
        return result;
    }
    
    public double getPrice(Product product, Promotion promotion){
        double price = -3;
        String query = "SELECT discount WHERE productid = ? AND promotionid = ?";
        try {
            conn = DBConnect.getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, product.getId());
                ps.setLong(2, promotion.getId());
                ResultSet rs = ps.executeQuery();
                if (rs.next()){
                    double discount = rs.getDouble("discount");
                    double discounted = product.getPrice() * discount;
                    price = product.getPrice() - discounted;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            price = -3;
        }
        return price;
    }

}
