/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cat.proven.wolprayproject.models.persist;

/**
 *  Queries of the ProductDao.
 * 
 *  @author Lewis
 */
public abstract class ProductQueries {
    
    public final static String SELECT_PROMOTES_BY_PROMOTION = "select * from products" +"LEFT JOIN promotes " 
                                                                + "on promotes.promotionid = products.productid WHERE "
                                                                + "promotes.promotionid = ?;";
    public final static String SELECT_ALL_PRODUCTS = "SELECT * FROM products";
    public final static String SELECT_PRODUCT_WHERE_NAME = "SELECT * FROM products WHERE LOWER(poductname)= ?";
    public final static String SELECT_PRODUCT_WHERE_ID = "SELECT * FROM products WHERE productid = ?";
    public final static String INSERT_NEW_PRODUCT = "INSERT INTO products values(?,?,?,?,?,?,?,?)";
    public final static String SELECT_ALL_PRODUCTS_IN_CLUB = "SELECT * FROM products WHERE clubid = ?";
    public final static String DELETE_PRODUCT_WHERE_ID = "DELETE FROM products WHERE productid = ?;";
    public final static String SELECT_PRODUCT_WHERE_CATEGORY = "SELECT * FROM products WHERE LOWER(category) = ?";
    public final static String SELECT_PRODUCT_WHERE_MORE_THAN = "SELECT * FROM products WHERE price >= ?";
    public final static String SELECT_PRODUCT_WHERE_LESS_THAN = "SELECT * FROM products WHERE price <= ?";
    public final static String UPDATE_PRODUCT = "UPDATE products SET productname = ?, description = ?, price = ?, category = ? WHERE productid = ?";
    
}
 