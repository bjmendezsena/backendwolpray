package org.cat.proven.wolprayproject.models.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.sql.Date;
import org.cat.proven.wolprayproject.models.pojo.Availability;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Product;
import org.cat.proven.wolprayproject.models.pojo.Reservation;
import org.cat.proven.wolprayproject.models.pojo.Slot;

/**
 * Manages access to database data.
 *
 * @author Lewis
 */
public class ClubDao {

    private Connection conn;

    /**
     * Empty builder.
     */
    public ClubDao() {
    }

    /**
     * Search all clubs from the database.
     *
     * @return list of clubs from the database.
     */
    public List<Club> findAllClubs() {
        List<Club> result;
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            if (conn != null) {
                Statement smt = conn.createStatement();
                ResultSet rs = smt.executeQuery(ClubQueries.SELECT_ALL_CLUBS);
                while (rs.next()) {
                    Club club = getClub(rs);
                    if (club != null) {
                        result.add(club);
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQL EXCEPTION: " + ex);
            result = null;
        }
        return result;
    }

    /**
     * Find club from the database with a given id from the controller.
     *
     * @param id:Id of the club to find.
     * @return Club object, null in case of error.
     */
    public Club findClubById(long id) {
        Club result = null;
        try {

            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_ID);

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                result = getClub(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Saltó la excepcion: " + ex.getMessage());
            result = null;
        }

        return result;
    }

    /**
     *
     * Find club from the database with a given name from the controller.
     *
     * @param name: name of the club to find.
     * @return Club object, null in case of error.
     */
    public Club findClubByName(String name) {
        Club result = null;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_NAME);
            ps.setString(1, name.toLowerCase());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = getClub(rs);
            }
        } catch (SQLException ex) {
            result = null;
        }
        return result;
    }

    /**
     * Find a listo of clubs from the database with a given city from the
     * controller.
     *
     * @param city: City of the clubs to find.
     * @return list of clubs from the database with a same attribute, null in
     * case of error.
     */
    public List<Club> findClubsBycity(String city) {

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_CITY);
            ps.setString(1, city.toLowerCase());
            ResultSet rs = ps.executeQuery();
            List<Club> result = new ArrayList<>();
            while (rs.next()) {
                Club club = getClub(rs);
                if (club != null) {
                    result.add(club);
                }
            }
            if (result.size() > 0) {
                return result;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Find a listo of clubs from the database with a given ambience from the
     * controller.
     *
     * @param ambience: Ambience of the clubs to find.
     * @return return list of clubs from the database with a same attribute,
     * null in case of error.
     */
    public List<Club> findClubsByAmbience(String ambience) {
        List<Club> result;
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_AMBIENCE);
            ps.setString(1, ambience.toLowerCase());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Club club = getClub(rs);
                if (club != null) {
                    result.add(club);
                }
            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    /**
     * Find a listo of clubs from the database with a given dress code from the
     * controller.
     *
     * @param dressCode: Dress code of the clubs to find.
     * @return return list of clubs from the database with a same attribute,
     * null in case of error.
     */
    public List<Club> findClubsByDressCode(String dressCode) {
        List<Club> result;
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_DRESS_CODE);
            ps.setString(1, dressCode.toLowerCase());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Club club = getClub(rs);
                if (club != null) {
                    result.add(club);
                }
            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    /**
     * Find a listo of clubs from the database with a given postal code from the
     * controller.
     *
     * @param cp: Postal code of the clubs to find.
     * @return return list of clubs from the database with a same attribute,
     * null in case of error.
     */
    public List<Club> findClubsByPostalCode(String cp) {
        List<Club> result;
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_WHERE_POSTAL_CODE);
            ps.setString(1, cp);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Club club = getClub(rs);
                if (club != null) {
                    result.add(club);
                }
            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public List<Club> findClubsByManager(int id) {
        List<Club> result;
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.SELECT_CLUB_BY_MANAGER);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long ids = rs.getLong("clubid");
                Club club = findClubById(ids);
                if (club != null) {
                    result.add(club);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    /**
     * Construct a club object with a given result set.
     *
     * @param rs : Result set with which to build the club object.
     * @return club object, null in case of error.
     */
    private Club getClub(ResultSet rs) {
        Club club;
        try {
            int id = rs.getInt("clubid");
            String name = rs.getString("clubname");
            String streetName = rs.getString("streetname");
            String streetNumber = rs.getString("streetnumber");
            String postalCode = rs.getString("postal_code");
            String city = rs.getString("city");
            String description = rs.getString("description");
            String ambience = rs.getString("ambience");
            String phone = rs.getString("phone");
            String dressCode = rs.getString("dress_code");
            String coverUrl = rs.getString("cover_url");
            String latitude = rs.getString("latitude");
            String longitude = rs.getString("longitude");
            List<Slot> schedule = getSchedule(id);
            Availability availability = getAvailability(id);

            if (schedule != null && availability != null) {
                club = new Club(id, name, streetName, streetNumber, postalCode, city, description, ambience, phone, dressCode,
                        coverUrl, latitude, longitude, schedule, availability);
            } else {
                club = new Club(id, name, streetName, streetNumber, postalCode, city, description, ambience, phone, dressCode,
                        coverUrl, latitude, longitude);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            club = null;
        }
        return club;
    }

    /**
     * Find a listo of slots from the database with a given club id from the
     * controller.
     *
     * @param id: Id of the slots to find.
     * @return list of slots from the database with a same clubid, null in case
     * of error.
     */
    private List<Slot> getSchedule(int id) {
        List<Slot> result;
        String query = "SELECT * FROM slots WHERE clubid = ?";
        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Slot slot = getSlot(rs);
                if (slot != null) {
                    result.add(slot);
                }
            }
        } catch (SQLException ex) {
            result = null;
        }

        return result;
    }

    /**
     * Construct a slot object with a given result set.
     *
     * @param rs : Result set with which to build the slot object.
     * @return slot object, null in case of error.
     */
    private Slot getSlot(ResultSet rs) {
        Slot slot;

        try {
            int slotId = rs.getInt("slotid");
            int clubId = rs.getInt("clubid");
            String openingTime = rs.getString("opening_time");
            String closingTime = rs.getString("closing_time");
            String day = rs.getString("day");
            slot = new Slot(slotId, clubId, openingTime, closingTime, day);
        } catch (SQLException e) {
            slot = null;
        }
        return slot;
    }

    private Availability getAvailability(int id) {
        Availability availability = null;
        String query = "SELECT * FROM availability WHERE clubid = ?";
        boolean isOpen, reservationAvailable, fullCapacity;
        try {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int is_open = rs.getInt("is_open");
                int reservation_available = rs.getInt("reservation_available");
                int its_full_capacity = rs.getInt("its_full_capacity");

                if (is_open == 1) {
                    isOpen = true;
                } else {
                    isOpen = false;
                }

                if (reservation_available == 1) {
                    reservationAvailable = true;
                } else {
                    reservationAvailable = false;
                }

                if (its_full_capacity == 1) {
                    fullCapacity = true;
                } else {
                    fullCapacity = false;
                }
                availability = new Availability(isOpen, reservationAvailable, fullCapacity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            availability = null;
        }
        return availability;
    }

    public boolean addNewClub(Club club) {
        boolean result = false;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.INSERT_NEW_CLUB);

            ps.setLong(1, club.getId());
            ps.setString(2, club.getName());
            ps.setString(3, club.getStreetName());
            ps.setString(4, club.getStreetNumber());
            ps.setString(5, club.getPostalCode());
            ps.setString(6, club.getCity());
            ps.setString(7, club.getDescription());
            ps.setString(8, club.getAmbience());
            ps.setString(9, club.getPhone());
            ps.setString(10, club.getDressCode());
            ps.setString(11, "/assets/clubs/" + club.getCoverUrl());
            ps.setString(12, club.getLatitude());
            ps.setString(13, club.getLongitude());
            if (ps.executeUpdate() > 0) {
                result = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }

    public boolean deleteClub(Club club) {
        boolean result = false;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.DELETE_CLUB);
            ps.setLong(1, club.getId());
            if (ps.executeUpdate() > 0) {
                result = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }

    public boolean modifyClub(Club club) {
        boolean result = false;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ClubQueries.UPDATE_CLUB);
            ps.setString(1, club.getName());
            ps.setString(2, club.getStreetName());
            ps.setString(3, club.getStreetNumber());
            ps.setString(4, club.getPostalCode());
            ps.setString(5, club.getCity());
            ps.setString(6, club.getDescription());
            ps.setString(7, club.getAmbience());
            ps.setString(8, club.getPhone());
            ps.setString(9, club.getDressCode());
            ps.setString(10, club.getLatitude());
            ps.setString(11, club.getLongitude());
            ps.setLong(12, Long.parseLong(String.valueOf(club.getId())));
            if (ps.executeUpdate() > 0) {
                result = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            result = false;
        }

        return result;
    }

    public int getClubId() {
        int result = -1;
        String query = "SELECT max(clubid) as id FROM clubs;";

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException ex) {
            result = -1;
        }
        return result ;
    }
}
