
package org.cat.proven.wolprayproject.models.persist;

/**
 * Queries of the ReseravtonDao.
 * 
 * @author Lewis
 */
public abstract class ReservationQueries {
    public final static String SELECT_ALL_RESERVATIONS = "SELECT * FROM reservations";
    public final static String SELECT_ALL_RESERVATIONS_WHERE_USER = "SELECT * FROM reservations WHERE userid = ?";
    public final static String SELECT_ALL_RESERVATIONS_WHERE_STATE = "SELECT * FROM reservations WHERE LOWER(state) = ?";
    public final static String SELECT_RESERVATION_WHERE_ID = "SELECT * FROM reservations WHERE reservid = ?";
    public final static String SELECT_ALL_RESERVATIONS_WHERE_CLUB = "SELECT * FROM reservations WHERE clubid = ?";
    public final static String SELECT_ALL_RESERVATIONS_WHERE_DATE = "SELECT * FROM reservations WHERE date = ?";
    public final static String INSERT_RESERVATION = "INSERT INTO reservations(clubid, userid, n_people, notes, date, state) VALUES(?,?,?,?,?,?)";
    public final static String DELETE_RESERVATIONS_WHERE_TABLE = "DELETE FROM reservations WHERE reservationid = ?";
    public final static String UPDATE_RESERVATION = "UPDATE reservations SET n_people = ?, notes = ?, date = ? , state = ? WHERE reservid = ?";
    
}
