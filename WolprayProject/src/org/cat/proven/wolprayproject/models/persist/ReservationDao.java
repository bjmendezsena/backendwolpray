package org.cat.proven.wolprayproject.models.persist;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Reservation;
import org.cat.proven.wolprayproject.models.pojo.User;

/**
 * Manages access to database data.
 *
 * @author Lewis
 */
public class ReservationDao {

    private Connection conn;

    /**
     * Empty builder.
     */
    public ReservationDao() {
    }

    /**
     * Add a list of reservations given from the controller.
     *
     * @param reservationList: Reservations list to add.
     * @return True if adds them all, false otherwise.
     */
    public boolean addReservation(Reservation reservation) {
        boolean itsReserved;

        try {
            conn = DBConnect.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.INSERT_RESERVATION);

            ps.setLong(1, reservation.getClubId());
            ps.setInt(2, reservation.getUserId());
            ps.setInt(3, reservation.getN_people());
            ps.setString(4, reservation.getNotes());
            ps.setString(5, reservation.getDate());
            ps.setString(6, reservation.getStatus());
            ps.executeUpdate();

            conn.commit();
            itsReserved = true;

        } catch (SQLException e) {
            try {
                conn.rollback();

            } catch (SQLException ex) {
            }
            itsReserved = false;
        }
        return itsReserved;
    }

    /**
     * Find a reservation list from the database with a given user from the
     * controller.
     *
     * @param user: User who has made reservations.
     * @return list of reservation from the database with a same attribute, null
     * in case of error.
     */
    public List<Reservation> findReservationsByUser(User user) {
        List<Reservation> result = new ArrayList<>();
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_ALL_RESERVATIONS_WHERE_USER);
            ps.setLong(1, user.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = getReservation(rs);
                if (reservation != null) {
                    result.add(reservation);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    /**
     * Find a reservation list from the database with a given date from the
     * controller.
     *
     * @param date: Date of reservations.
     * @param clubId: Club id of reservations.
     * @return list of reservation from the database with a same attribute, null
     * in case of error.
     */
    public List<Reservation> findReservationsByDate(String date) {
        List<Reservation> result = new ArrayList<>();

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_ALL_RESERVATIONS_WHERE_DATE);
            ps.setString(1, date.trim());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = getReservation(rs);
                if (reservation != null) {
                    result.add(reservation);
                }
            }

        } catch (SQLException e) {
            result = null;
        }

        return result;
    }

    public List<Reservation> findReservationsByState(String state) {
        List<Reservation> result = new ArrayList<>();

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_ALL_RESERVATIONS_WHERE_STATE);
            ps.setString(1, state.toLowerCase());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = getReservation(rs);
                if (reservation != null) {
                    result.add(reservation);
                }
            }

        } catch (SQLException e) {
            result = null;
        }

        return result;
    }

    public Reservation findReservationsById(int id) {
        Reservation result = null;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_RESERVATION_WHERE_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = getReservation(rs);
            }

        } catch (SQLException e) {
            result = null;
        }

        return result;
    }

    public boolean modifyReservation(Reservation reservation) {
        boolean result = false;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.UPDATE_RESERVATION);
            ps.setInt(1, reservation.getN_people());
            ps.setString(2, reservation.getNotes());
            ps.setString(3, reservation.getDate());
            ps.setString(4, reservation.getStatus());
            ps.setInt(5, reservation.getReservId());
            if (ps.executeUpdate() > 0) {
                result = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }

        return result;
    }

    /**
     * Find a reservation list from the database with a given club from the
     * controller.
     *
     * @param club: club of reservations.
     * @return list of reservation from the database with a same attribute, null
     * in case of error.
     */
    public List<Reservation> findReservationsByClub(Club club) {
        List<Reservation> result = new ArrayList<>();

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_ALL_RESERVATIONS_WHERE_CLUB);
            ps.setLong(1, club.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = getReservation(rs);
                if (reservation != null) {
                    result.add(reservation);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    public List<Reservation> findAllReservations() {
        List<Reservation> result = new ArrayList<>();

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.SELECT_ALL_RESERVATIONS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reservation reservation = getReservation(rs);
                if (reservation != null) {
                    result.add(reservation);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }

    public boolean removeReservations(Reservation... reservations) {
        boolean isRemoved = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(ReservationQueries.DELETE_RESERVATIONS_WHERE_TABLE);

            for (Reservation reservation : reservations) {
                ps.setLong(1, reservation.getClubId());
                ps.executeUpdate();
            }

            isRemoved = true;
        } catch (SQLException e) {
            e.printStackTrace();
            isRemoved = false;
        }

        return isRemoved;
    }

    private Reservation getReservation(ResultSet rs) {
        Reservation reservation;

        try {
            int reservId = rs.getInt("reservid");
            int clubid = rs.getInt("clubid");
            int userid = rs.getInt("userid");
            int n_people = rs.getInt("n_people");
            String notes = rs.getString("notes");
            Date date = rs.getDate("date");
            String dates = date.toString();
            String state = rs.getString("state");
            reservation = new Reservation(reservId, clubid, userid, n_people, notes, dates, state);
        } catch (SQLException e) {
            e.printStackTrace();
            reservation = null;
        }
        return reservation;
    }

}
