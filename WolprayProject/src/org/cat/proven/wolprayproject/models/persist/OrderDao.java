package org.cat.proven.wolprayproject.models.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.cat.proven.wolprayproject.models.pojo.Club;
import org.cat.proven.wolprayproject.models.pojo.Order;
import org.cat.proven.wolprayproject.models.pojo.Product;
import org.cat.proven.wolprayproject.models.pojo.Promotion;
import org.cat.proven.wolprayproject.models.pojo.User;

/**
 *
 * @author Lewis
 */
public class OrderDao {

    private Connection conn;

    /**
     * Empty builder.
     */
    public OrderDao() {
    }

    public List<Order> findAllOrders() {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            if (conn != null) {
                Statement smt = conn.createStatement();
                ResultSet rs = smt.executeQuery(OrderQueries.SELECT_ALL_ORDERS);

                while (rs.next()) {
                    Order order = getOrder(rs);
                    if (order != null) {
                        result.add(order);
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("SQL EXCEPTION: " + ex);
            result = null;
        }

        return result;
    }

    public List<Order> findByClub(Club club) {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_CLUB);
            ps.setLong(1, club.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = getOrder(rs);
                if (order != null) {
                    result.add(order);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public List<Order> findByUser(User user) {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_USER);
            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = getOrder(rs);
                if (order != null) {
                    result.add(order);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public List<Order> findByProduct(Product product) {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_PRODUCT);
            ps.setLong(1, product.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = getOrder(rs);
                if (order != null) {
                    result.add(order);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public List<Order> findByPromotion(Promotion promotion) {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_PROMOTION);
            ps.setLong(1, promotion.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = getOrder(rs);
                if (order != null) {
                    result.add(order);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public List<Order> findByStatus(String status) {
        List<Order> result;

        try {
            result = new ArrayList<>();
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_STATUS);
            ps.setString(1, status);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = getOrder(rs);
                if (order != null) {
                    result.add(order);
                }

            }
        } catch (SQLException e) {
            result = null;
        }
        return result;
    }

    public Order findById(int id) {
        Order order = null;

        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.SELECT_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                order = getOrder(rs);
            }
        } catch (SQLException e) {
            order = null;
        }
        return order;
    }

    private Order getOrder(ResultSet rs) {
        Order order = null;
        try {
            int orderId = rs.getInt("orderid");
            long clubId = rs.getLong("clubid");
            int userId = rs.getInt("userid");
            long productId = rs.getLong("productid");
            long promotionId = rs.getLong("promotionid");
            double price = rs.getDouble("price");
            String status = rs.getString("status");

            order = new Order(orderId, clubId, userId, productId, promotionId, price, status);

        } catch (SQLException ex) {
            ex.printStackTrace();
            order = null;
        }

        return order;
    }

    public boolean addOrder(Order order) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.INSERT_NEW_ORDER);
            ps.setLong(1, order.getClubId());
            ps.setInt(2, order.getUserId());
            ps.setLong(3, order.getProductId());
            ps.setLong(4, order.getPromotionId());
            ps.setDouble(5, order.getPrice());
            ps.setString(6, order.getStatus());
            if(ps.executeUpdate() > 0){
                result = true;
            }
        } catch (SQLException e) {
            result = false;
        }
        return result;
    }
    
    public boolean addOrderWithNoPromotion(Order order) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.INSERT_NEW_ORDER_WITH_NO_PROMOTION);
            ps.setLong(1, order.getClubId());
            ps.setInt(2, order.getUserId());
            ps.setLong(3, order.getProductId());
            ps.setDouble(4, order.getPrice());
            ps.setString(5, order.getStatus());
            if(ps.executeUpdate() > 0){
                result = true;
            }
        } catch (SQLException e) {
            result = false;
        }
        return result;
    }
    
    public boolean changeOrder(Order order) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.CHANGE_ORDER);
            ps.setString(1, order.getStatus());
            ps.setLong(2, order.getClubId());
            if(ps.executeUpdate() > 0){
                result = true;
            }
        } catch (SQLException e) {
            result = false;
        }
        return result;
    }
    
    public boolean deleteOrder(Order order) {
        boolean result = false;
        try {
            conn = DBConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(OrderQueries.DELETE_ORDER);
            ps.setInt(1, order.getOrderId());
            if(ps.executeUpdate() > 0){
                result = true;
            }
        } catch (SQLException e) {
            result = false;
        }
        return result;
    }
}
