
package org.cat.proven.wolprayproject.models.persist;


/**
 *  Queries of the ClubDao.
 * 
 * @author Lewis
 */
public abstract class ClubQueries {
    public final static String SELECT_ALL_CLUBS = "SELECT * FROM clubs";
    public final static String SELECT_CLUB_WHERE_ID = "SELECT * FROM clubs WHERE clubid = ?";
    public final static String SELECT_CLUB_WHERE_NAME = "SELECT * FROM clubs WHERE LOWER(clubname) = ?";
    public final static String SELECT_CLUB_WHERE_CITY = "SELECT * FROM clubs WHERE LOWER(city) = ?";
    public final static String SELECT_CLUB_WHERE_AMBIENCE = "SELECT * FROM clubs WHERE LOWER(ambience) = ?";
    public final static String SELECT_CLUB_WHERE_DRESS_CODE = "SELECT * FROM clubs WHERE LOWER(dress_code) = ?";
    public final static String SELECT_CLUB_WHERE_POSTAL_CODE = "SELECT * FROM clubs WHERE postal_code = ?";
    public final static String SELECT_CLUB_BY_MANAGER = "SELECT clubid FROM managers WHERE userid = ?";
    public final static String INSERT_NEW_CLUB = "INSERT INTO clubs VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public final static String DELETE_CLUB = "DELETE FROM clubs WHERE clubid = ?";
    public final static String UPDATE_CLUB = "UPDATE clubs SET "
            + "clubname = ? ,"
            + "streetname = ?,"
            + "streetnumber = ?,"
            + "postal_code = ?,"
            + "city = ?,"
            + "description = ?,"
            + "ambience = ?,"
            + "phone = ?,"
            + "dress_code = ?,"
            + "latitude = ?,"
            + "longitude = ? "
            + "WHERE clubid = ?";
    
}
