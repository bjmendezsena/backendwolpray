
package org.cat.proven.wolprayproject.models.persist;

/**
 *
 * @author Lewis
 */
public class OrderQueries {
    public final static String SELECT_ALL_ORDERS = "SELECT * FROM orders";
    public final static String SELECT_BY_ID = "SELECT * FROM orders WHERE orderid = ?";
    public final static String SELECT_BY_CLUB = "SELECT * FROM orders WHERE clubid = ?";
    public final static String SELECT_BY_USER = "SELECT * FROM orders WHERE userid = ?";
    public final static String SELECT_BY_PRODUCT = "SELECT * FROM orders WHERE productid = ?";
    public final static String SELECT_BY_PROMOTION = "SELECT * FROM orders WHERE promotionid = ?";
    public final static String SELECT_BY_STATUS = "SELECT * FROM orders WHERE status = ?";
    public final static String INSERT_NEW_ORDER = "INSERT INTO orders(clubid, userid, producid, promotionid, price, status) VALUES(?,?,?,?,?,?)";
    public final static String INSERT_NEW_ORDER_WITH_NO_PROMOTION = "INSERT INTO orders(clubid, userid, producid, price, status) VALUES(?,?,?,?,?)";
    public final static String CHANGE_ORDER = "UPDATE orders SET status = ? WHERE orderid = ?";
    public final static String DELETE_ORDER = "DELETE orders WHERE orderid = ?";
}
